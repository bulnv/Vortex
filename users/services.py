import logging

from users.models import UserPublic

logger = logging.getLogger(__name__)


def mark_user_account_verified(user: UserPublic) -> UserPublic:
    """Activate user account."""
    # FIXME: need to add check if user has been banned and raise an error if it has.
    logger.info("Marking user as verified, user %s", user)
    if user.is_verified:
        logger.info("User %s already verified", user)
        return user
    user.is_verified = True
    user.save()
    return user


def mark_user_account_unverified(user: UserPublic) -> UserPublic:
    """Deactivate user account."""
    logger.info("Marking user as unverified, user %s", user)
    if not user.is_verified:
        logger.info("User %s already unverified", user)
        return user
    user.is_verified = False
    user.save()
    return user
