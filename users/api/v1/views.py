from drf_spectacular.utils import extend_schema
from rest_framework import mixins, status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from common.api import parameters
from users.api.permissions import Authenticator, LoadtestWorker, OwnUser
from users.api.serializers import UserPublicCreateSerializer, UserPublicFullSerializer
from users.selectors import fetch_users
from users.services import mark_user_account_unverified, mark_user_account_verified


class UserViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    GenericViewSet,
):
    """API to manage user profile."""

    queryset = fetch_users()
    serializer_class = UserPublicFullSerializer
    lookup_field = "username"
    http_method_names = ["post", "get", "patch"]
    permission_classes = [Authenticator | LoadtestWorker]

    def get_permissions(self):
        """Return proper permissions based on action user performing."""
        if self.action in ("create", "verify", "unverify", "destroy", "delete"):
            permission = Authenticator | LoadtestWorker
            return [permission()]
        elif self.action in ("update", "partial_update"):
            permission = Authenticator | LoadtestWorker | OwnUser
            return [permission()]
        return [AllowAny()]

    def get_serializer_class(self):
        """Return serializer class based on action."""
        if self.action == "create":
            return UserPublicCreateSerializer
        return self.serializer_class

    @extend_schema(parameters=[parameters.INTERNAL_TOKEN])
    def create(self, request, *args, **kwargs):
        """Создает профиль юзера, принимает только запросы из других сервисов."""
        return super().create(request, *args, **kwargs)

    @extend_schema(parameters=[parameters.INTERNAL_TOKEN])
    @action(methods=["POST"], detail=True, serializer_class=UserPublicCreateSerializer)
    def verify(self, request, *args, **kwargs):
        """Отмечает пользователя как is_verified, только запросы из других сервисов."""
        user = self.get_object()
        user = mark_user_account_verified(user)
        return Response(self.get_serializer(user).data, status=status.HTTP_200_OK)

    @extend_schema(parameters=[parameters.INTERNAL_TOKEN])
    @action(methods=["POST"], detail=True, serializer_class=UserPublicCreateSerializer)
    def unverify(self, request, *args, **kwargs):
        """Отменяет верификацию пользователя, только запросы из других сервисов."""
        user = self.get_object()
        user = mark_user_account_unverified(user)
        return Response(self.get_serializer(user).data, status=status.HTTP_200_OK)
