from rest_framework import serializers

from users.models import UserPublic


class UserPublicCreateSerializer(serializers.ModelSerializer):
    """Serializer to create user account via auth service."""

    class Meta:
        model = UserPublic
        fields = (
            "external_user_uid",
            "username",
            "email",
            "is_verified",
        )


class UserPublicFullSerializer(serializers.ModelSerializer):
    """Serializer to represent whole user info."""

    votes_up_count = serializers.IntegerField(source="_votes_up_count", read_only=True)
    votes_down_count = serializers.IntegerField(
        source="_votes_down_count", read_only=True
    )
    comments_count = serializers.IntegerField(source="_comments_count", read_only=True)
    posts_count = serializers.IntegerField(source="_posts_count", read_only=True)
    registered_at = serializers.DateTimeField(source="created_at", read_only=True)

    class Meta:
        model = UserPublic
        fields = (
            "external_user_uid",
            "username",
            "date_of_birth",
            "avatar",
            "bio",
            "rating",
            "posts_count",
            "comments_count",
            "votes_up_count",
            "votes_down_count",
            "is_verified",
            "registered_at",
        )
        read_only_fields = (
            "external_user_uid",
            "username",
            "rating",
            "posts_count",
            "comments_count",
            "votes_up_count",
            "votes_down_count",
            "is_verified",
            "created_at",
        )


class UserPublicMinimalSerializer(serializers.ModelSerializer):
    """Serializer to represent minimum info about user in posts."""

    class Meta:
        model = UserPublic
        fields = (
            "username",
            "avatar",
        )
