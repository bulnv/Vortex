import uuid

import factory
from factory.django import DjangoModelFactory


class UserPublicFactory(DjangoModelFactory):
    external_user_uid = factory.Faker("uuid4")
    username = factory.Faker("uuid4")
    email = factory.LazyAttribute(lambda _: f"{uuid.uuid4()}@kapi.bar")
    is_active = True

    class Meta:
        model = "users.UserPublic"
