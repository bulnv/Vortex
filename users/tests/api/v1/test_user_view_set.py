import datetime
import uuid

import faker
import pytest
from rest_framework import status
from rest_framework.reverse import reverse

from comments.choices import Vote as CommentVoteChoice
from comments.tests.factories import CommentFactory, CommentVoteFactory
from posts.choices import PostStatus
from posts.choices import Vote as PostVoteChoice
from posts.tests.factories import PostFactory, PostVoteFactory
from users.models import UserPublic
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestUsersViewSet:
    def setup(self):
        self.internal_token_header = "X-Test-Header"
        self.internal_token = "test-token"
        self.username = "any-username"
        self.email = "test@kapi.bar"
        self.external_user_uid = str(uuid.uuid4())
        self.data = {
            "external_user_uid": self.external_user_uid,
            "username": self.username,
            "email": self.email,
        }

    def test_can_create_non_verified_user_using_service(self, anon_api_client, settings):
        settings.INTERNAL_TOKEN_HEADER = self.internal_token_header
        settings.INTERNAL_TOKENS = [self.internal_token]

        result = self._create_user(
            anon_api_client(),
            self.data,
            headers={self.internal_token_header: self.internal_token},
        )
        assert result.status_code == status.HTTP_201_CREATED
        data = result.data
        assert data["external_user_uid"] == self.external_user_uid
        assert data["username"] == self.username
        assert data["email"] == self.email
        assert data["is_verified"] is False
        user = UserPublic.objects.get(external_user_uid=self.external_user_uid)
        assert user.is_active is True
        assert user.is_verified is False

    @pytest.mark.parametrize(
        "action,initial_is_verified,post_action_is_verified",
        (
            ("verify", False, True),
            ("unverify", True, False),
        ),
    )
    def test_can_toggle_user_is_verified_using_service(
        self,
        action,
        initial_is_verified,
        post_action_is_verified,
        anon_api_client,
        settings,
    ):
        settings.INTERNAL_TOKEN_HEADER = self.internal_token_header
        settings.INTERNAL_TOKENS = [self.internal_token]

        user = UserPublicFactory(**self.data, is_verified=initial_is_verified)

        result = self._verify_unverify_user(
            action,
            anon_api_client(),
            user,
            headers={self.internal_token_header: self.internal_token},
        )
        user.refresh_from_db()
        assert result.status_code == status.HTTP_200_OK
        data = result.data
        assert data["external_user_uid"] == self.external_user_uid
        assert data["username"] == self.username
        assert data["email"] == self.email
        assert data["is_verified"] is post_action_is_verified
        assert user.is_verified is post_action_is_verified

    @pytest.mark.parametrize(
        "action,initial_is_verified", (("verify", False), ("unverify", True))
    )
    def test_cant_toggle_user_is_verified_with_wrong_header_name(
        self, action, initial_is_verified, anon_api_client, settings
    ):
        settings.INTERNAL_TOKENS = [self.internal_token]
        user = UserPublicFactory(**self.data, is_verified=initial_is_verified)
        result = self._verify_unverify_user(
            action,
            anon_api_client(),
            user,
            headers={"X-Wrong-Header": self.internal_token},
        )
        user.refresh_from_db()
        assert result.status_code == status.HTTP_401_UNAUTHORIZED
        assert user.is_verified is initial_is_verified

    @pytest.mark.parametrize(
        "action,initial_is_verified", (("verify", False), ("unverify", True))
    )
    def test_cant_toggle_is_verified_with_wrong_token(
        self, action, initial_is_verified, anon_api_client, settings
    ):
        settings.INTERNAL_TOKEN_HEADER = self.internal_token_header
        user = UserPublicFactory(**self.data, is_verified=initial_is_verified)
        result = self._verify_unverify_user(
            action,
            anon_api_client(),
            user,
            headers={self.internal_token_header: "wrong_value"},
        )
        user.refresh_from_db()
        assert result.status_code == status.HTTP_401_UNAUTHORIZED
        assert user.is_verified is initial_is_verified

    @pytest.mark.parametrize(
        "action,initial_is_verified", (("verify", False), ("unverify", True))
    )
    def test_cant_toggle_is_verified_without_headers(
        self, action, initial_is_verified, anon_api_client
    ):
        user = UserPublicFactory(**self.data, is_verified=initial_is_verified)
        result = self._verify_unverify_user(action, anon_api_client(), user)
        user.refresh_from_db()
        assert result.status_code == status.HTTP_401_UNAUTHORIZED
        assert user.is_verified is initial_is_verified

    @pytest.mark.parametrize("is_superuser", (True, False))
    @pytest.mark.parametrize("is_staff", (True, False))
    @pytest.mark.parametrize(
        "action,initial_is_verified", (("verify", False), ("unverify", True))
    )
    def test_cant_verify_as_logged_in_user(
        self, action, initial_is_verified, is_staff, is_superuser, authed_api_client
    ):
        user = UserPublicFactory(
            is_staff=is_staff,
            is_superuser=is_superuser,
            is_verified=initial_is_verified,
        )
        result = self._verify_unverify_user(action, authed_api_client(user), user)
        user.refresh_from_db()
        assert result.status_code == status.HTTP_403_FORBIDDEN
        assert user.is_verified is initial_is_verified

    def test_cant_create_with_wrong_header_name(self, anon_api_client, settings):
        settings.INTERNAL_TOKENS = [self.internal_token]
        result = self._create_user(
            anon_api_client(),
            self.data,
            headers={"X-Wrong-Header": self.internal_token},
        )
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def test_cant_create_with_wrong_token(self, anon_api_client, settings):
        settings.INTERNAL_TOKEN_HEADER = self.internal_token_header
        result = self._create_user(
            anon_api_client(),
            self.data,
            headers={self.internal_token_header: "wrong_value"},
        )
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def test_cant_create_without_headers(self, anon_api_client):
        result = self._create_user(anon_api_client(), self.data)
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    @pytest.mark.parametrize("is_superuser", (True, False))
    @pytest.mark.parametrize("is_staff", (True, False))
    def test_cant_create_as_logged_in_user(
        self, is_staff, is_superuser, authed_api_client
    ):
        user = UserPublicFactory(
            is_staff=is_staff,
            is_superuser=is_superuser,
        )
        result = self._create_user(authed_api_client(user), self.data)
        assert result.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize("is_verified", (True, False))
    @pytest.mark.parametrize("field_name", ("external_user_uid", "username", "email"))
    def test_cant_create_user_with_same_fields(
        self, field_name, is_verified, anon_api_client, settings
    ):
        settings.INTERNAL_TOKEN_HEADER = self.internal_token_header
        settings.INTERNAL_TOKENS = [self.internal_token]

        # Existing user
        UserPublicFactory(**self.data, is_verified=is_verified)

        # Creating fake data for a new user
        fake = faker.Faker()
        data = {
            "external_user_uid": str(uuid.uuid4()),
            "username": fake.user_name(),
            "email": fake.email(),
        }

        result = self._create_user(
            anon_api_client(),
            {
                **data,
                # replace field with data present on existing user
                field_name: self.data[field_name],
            },
            headers={self.internal_token_header: self.internal_token},
        )
        assert result.status_code == status.HTTP_400_BAD_REQUEST

    def test_can_update_own_account(self, authed_api_client):
        new_dob = "2020-01-01"
        new_bio = "new kapibio"
        user = UserPublicFactory()

        result = self._update_user(
            authed_api_client(user),
            user,
            data={
                "date_of_birth": new_dob,
                "bio": new_bio,
            },
        )
        user.refresh_from_db()

        data = result.data
        assert result.status_code == status.HTTP_200_OK
        assert data["date_of_birth"] == new_dob
        assert data["bio"] == new_bio
        assert f"{user.date_of_birth:%Y-%m-%d}" == new_dob
        assert user.bio == new_bio

    def test_cant_update_readonly_fields(self, authed_api_client):
        initial_rating = 1
        initial_comments_count = 0
        initial_votes_up_count = 0
        initial_votes_down_count = 0
        user = UserPublicFactory(rating=initial_rating)

        result = self._update_user(
            authed_api_client(user),
            user,
            data={
                "rating": 1000,
                "comments_count": 100,
                "votes_up_count": 200,
                "votes_down_count": 300,
            },
        )
        user.refresh_from_db()

        data = result.data
        assert result.status_code == status.HTTP_200_OK
        assert data["rating"] == initial_rating
        assert data["comments_count"] == initial_comments_count
        assert data["votes_up_count"] == initial_votes_up_count
        assert data["votes_down_count"] == initial_votes_down_count
        assert user.rating == initial_rating
        assert user.comments_count == initial_comments_count
        assert user.votes_up_count == initial_votes_up_count
        assert user.votes_down_count == initial_votes_down_count

    @pytest.mark.parametrize(
        "is_authenticated,expected_status_code",
        ((True, status.HTTP_403_FORBIDDEN), (False, status.HTTP_401_UNAUTHORIZED)),
    )
    def test_cant_update_other_account(
        self, is_authenticated, expected_status_code, authed_api_client, anon_api_client
    ):
        initial_date_of_birth = "2000-01-01"
        initial_bio = "viva la kapibara"
        other_user = UserPublicFactory(
            date_of_birth=initial_date_of_birth,
            bio=initial_bio,
        )
        if is_authenticated:
            client = authed_api_client(UserPublicFactory())
        else:
            client = anon_api_client()
        result = self._update_user(
            client,
            other_user,
            data={
                "date_of_birth": "2020-01-01",
                "bio": "new kapibio",
            },
        )
        other_user.refresh_from_db()

        assert result.status_code == expected_status_code
        assert f"{other_user.date_of_birth:%Y-%m-%d}" == initial_date_of_birth
        assert other_user.bio == initial_bio

    def test_get_proper_data_in_public_profile(self, anon_api_client):
        bio = "This is my bio"
        dob = datetime.date(2020, 1, 22)
        user = UserPublicFactory(
            bio=bio,
            date_of_birth=dob,
            is_verified=True,
        )
        PostFactory(user=user, status=PostStatus.DRAFT)
        PostFactory(user=user, status=PostStatus.DELETED)
        post = PostFactory(user=user, status=PostStatus.PUBLISHED)
        other_user_comment = CommentFactory(post=post)
        own_comment = CommentFactory(post=post, user=user)
        CommentVoteFactory(
            user=user, comment=other_user_comment, value=CommentVoteChoice.DOWNVOTE
        )
        CommentVoteFactory(user=user, comment=own_comment, value=CommentVoteChoice.UPVOTE)
        PostVoteFactory(user=user, post=post, value=PostVoteChoice.UPVOTE)

        result = self._get_user(anon_api_client(), user)
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        assert response["is_verified"] is True
        assert response["date_of_birth"] == dob.strftime("%Y-%m-%d")
        assert response["external_user_uid"] == user.external_user_uid
        assert response["posts_count"] == 1
        assert response["comments_count"] == 1
        assert response["votes_up_count"] == 2
        assert response["votes_down_count"] == 1

    def _create_user(self, client, data, headers=None):
        return client.post(reverse("v1:users:users-list"), data=data, headers=headers)

    def _verify_unverify_user(self, action, client, user, headers=None):
        return client.post(
            reverse(
                f"v1:users:users-{action}",
                kwargs={"username": user.username},
            ),
            headers=headers,
        )

    def _update_user(self, client, user, data):
        return client.patch(
            reverse(
                "v1:users:users-detail",
                kwargs={"username": user.username},
            ),
            data=data,
        )

    def _get_user(self, client, user):
        return client.get(
            reverse(
                "v1:users:users-detail",
                kwargs={"username": user.username},
            ),
        )
