from django.db.models import Count, Q

from comments.choices import Vote as CommentVoteChoice
from posts.choices import PostStatus
from posts.choices import Vote as PostVoteChoice
from users.models import UserPublic


def fetch_users():
    return UserPublic.objects.filter(is_active=True).annotate(
        _votes_up_count=(
            Count(
                "post_votes",
                Q(post_votes__value=PostVoteChoice.UPVOTE),
                distinct=True,
            )
            + Count(
                "comment_votes",
                Q(comment_votes__value=CommentVoteChoice.UPVOTE),
                distinct=True,
            )
        ),
        _votes_down_count=(
            Count(
                "post_votes",
                Q(post_votes__value=PostVoteChoice.DOWNVOTE),
                distinct=True,
            )
            + Count(
                "comment_votes",
                Q(comment_votes__value=CommentVoteChoice.DOWNVOTE),
                distinct=True,
            )
        ),
        _comments_count=Count("comments", distinct=True),
        _posts_count=Count("posts", Q(posts__status=PostStatus.PUBLISHED), distinct=True),
    )
