from typing import Annotated, Literal

from pydantic import BaseModel, Field, HttpUrl, conlist, field_validator

from common.helpers import sanitize_html_content


class ContentTypeHtml(BaseModel):
    type_: Literal["html"] = Field(..., alias="type")
    value: str

    @field_validator("value")
    @classmethod
    def check_value(cls, value: str) -> str:
        return sanitize_html_content(value)


class ContentTypeImg(BaseModel):
    type_: Literal["image"] = Field(..., alias="type")
    src: HttpUrl | str
    width: int | None = Field(None, ge=1, le=2048)
    height: int | None = Field(None, ge=1, le=20480)


ContentType = Annotated[
    ContentTypeImg | ContentTypeHtml, Field(..., discriminator="type_")
]


class Content(BaseModel):
    content: conlist(ContentType, min_length=1)
