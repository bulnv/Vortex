import uuid

import pydantic_core
from drf_spectacular.utils import OpenApiExample, extend_schema_serializer
from rest_framework import serializers

from comments.choices import Vote
from comments.models import Comment, CommentVote
from comments.selectors import can_edit_comment, get_children_comments
from common.schemas import Content
from posts.models import Post
from users.api.serializers import UserPublicMinimalSerializer


class CommentSerializer(serializers.ModelSerializer):
    """Serializer to represent comment."""

    author = UserPublicMinimalSerializer(source="user")
    children = serializers.SerializerMethodField()
    can_edit = serializers.SerializerMethodField()
    votes_up_count = serializers.IntegerField(source="_votes_up_count")
    votes_down_count = serializers.IntegerField(source="_votes_down_count")
    rating = serializers.FloatField(source="_rating")
    voted = serializers.ChoiceField(
        choices=Vote.choices, allow_null=True, source="_voted"
    )

    class Meta:
        model = Comment
        fields = (
            "uuid",
            "author",
            "content",
            "rating",
            "votes_up_count",
            "votes_down_count",
            "voted",
            "created_at",
            "level",
            "children",
            "can_edit",
        )
        read_only_fields = fields

    def get_children(self, obj: Comment):
        try:
            user = self.context["request"].user
        except (KeyError, AttributeError):
            user = None
        return CommentSerializer(
            get_children_comments(user, obj, max_level=self.context.get("max_level")),
            many=True,
            context=self.context,
        ).data

    def get_can_edit(self, obj: Comment):
        request = self.context.get("request")
        user = request and request.user
        return can_edit_comment(user, obj)


@extend_schema_serializer(
    examples=[
        OpenApiExample(
            name="Постинг ответа на коммент",
            value={
                "post": "post-slug",
                "parent": uuid.uuid4(),
                "content": [
                    {"type": "html", "value": "<p>Комментарий</p>"},
                    {
                        "type": "img",
                        "src": "https://path.to/image.jpg",
                        "width": 300,
                        "height": 400,
                    },
                ],
            },
            request_only=True,
        ),
        OpenApiExample(
            name="Постинг коммента к посту",
            value={
                "post": "post-slug",
                "content": [
                    {"type": "html", "value": "<p>Комментарий</p>"},
                    {
                        "type": "img",
                        "src": "https://path.to/image.jpg",
                        "width": 300,
                        "height": 400,
                    },
                ],
            },
            request_only=True,
        ),
    ]
)
class CommentCreateSerializer(serializers.ModelSerializer):
    """Serializer to create comment."""

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    author = UserPublicMinimalSerializer(source="user", read_only=True)
    post = serializers.SlugRelatedField(
        slug_field="slug", queryset=Post.objects.only("pk")
    )
    parent = serializers.SlugRelatedField(
        slug_field="uuid", queryset=Comment.objects.only("pk"), required=False
    )

    class Meta:
        model = Comment
        fields = (
            "uuid",
            "user",
            "author",
            "post",
            "parent",
            "content",
        )

        read_only_fields = (
            "uuid",
            "author",
        )

    def validate(self, attrs):
        """Validate comment data, make sure parent comment from the same post."""
        if parent := attrs.get("parent"):
            post = attrs["post"]
            if parent.post_id != post.pk:
                raise serializers.ValidationError(
                    "Нельзя отвечать на комментарий из другого поста!"
                )
        return super().validate(attrs)

    def validate_content(self, value):
        try:
            value = Content(content=value).model_dump(by_alias=True, exclude_none=True)
        except pydantic_core.ValidationError:
            raise serializers.ValidationError("Неверный формат данных")
        return value["content"]


class CommentUpdateSerializer(serializers.ModelSerializer):
    author = UserPublicMinimalSerializer(source="user", read_only=True)

    class Meta:
        model = Comment
        fields = (
            "uuid",
            "author",
            "content",
        )

        read_only_fields = (
            "uuid",
            "author",
        )


class CommentVoteCreateSerializer(serializers.ModelSerializer):
    """Serializer validating data submitted to cast a vote on a comment."""

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = CommentVote
        fields = (
            "user",
            "value",
        )


class CommentRatingOnlySerializer(serializers.ModelSerializer):
    """Serializer to return only rating related data."""

    class Meta:
        model = Comment
        fields = (
            "uuid",
            "votes_up_count",
            "votes_down_count",
            "rating",
        )
