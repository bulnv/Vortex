import datetime
import uuid
from unittest.mock import patch

import pytest
from django.utils import timezone
from django.utils.http import urlencode
from freezegun import freeze_time
from funcy import first
from rest_framework import status
from rest_framework.reverse import reverse

from comments.tests.factories import CommentFactory, CommentVoteFactory
from posts.tests.factories import PostFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestCommentViewSet:
    def setup(self):
        self.initial_post_comments_count = 5
        self.post = PostFactory(comments_count=self.initial_post_comments_count)
        self.initial_user_comments_count = 10
        self.user = UserPublicFactory(comments_count=self.initial_user_comments_count)
        self.content = [{"type": "html", "value": "test comment"}]

    def test_active_user_can_post_comment(self, authed_api_client):
        result = self._post_comment(
            authed_api_client(self.user), self.post, {"content": self.content}
        )
        data = result.data
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        assert data["author"]["username"] == self.user.username
        assert data["content"] == self.content

        self.post.refresh_from_db()
        self.user.refresh_from_db()
        assert self.post.comments.count() == 1
        assert self.post.comments_count == self.initial_post_comments_count + 1
        assert self.user.comments_count == self.initial_user_comments_count + 1

    @pytest.mark.parametrize(
        "content", ("", [], "test", {"type": "html", "value": "comment"})
    )
    def test_cant_post_comment_with_malformed_content(self, content, authed_api_client):
        result = self._post_comment(
            authed_api_client(self.user), self.post, {"content": content}
        )
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()

    def test_malicious_html_tags_in_comment_content_gets_cleaned(self, authed_api_client):
        content_with_tags = (
            "Test <b>html</b>"
            "<script>alert(1);</script>"
            "<br/>"
            "<h1>h1 tag</h1>"
            "<h2>h2 tag</h2>"
            "<h3>h3 tag</h3>"
            "<h4>h4 tag</h4>"
            "<h5>h5 tag</h5>"
            "<h6>h6 tag</h6>"
            "<a href='https://kapi.bar'>link</a>"
            "<a href='ftp://kapi.bar'>link</a>"
        )
        expected_sanitized_content = (
            "Test <b>html</b>"
            "<br>"
            "h1 tag"
            "h2 tag"
            "h3 tag"
            "h4 tag"
            "h5 tag"
            "h6 tag"
            '<a href="https://kapi.bar" rel="noopener noreferrer nofollow">link</a>'
            '<a rel="noopener noreferrer nofollow">link</a>'
        )
        result = self._post_comment(
            authed_api_client(self.user),
            self.post,
            {"content": [{"type": "html", "value": content_with_tags}]},
        )
        data = result.data
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        assert data["content"][0]["value"] == expected_sanitized_content

    def test_not_active_user_cant_post_comment(self, authed_api_client):
        user = UserPublicFactory(is_active=False)
        result = self._post_comment(
            authed_api_client(user), self.post, {"content": self.content}
        )
        assert result.status_code == status.HTTP_401_UNAUTHORIZED, result.content.decode()
        assert self.post.comments.count() == 0

    def test_not_logged_in_user_cant_post_comment(self, anon_api_client):
        result = self._post_comment(
            anon_api_client(), self.post, {"content": self.content}
        )
        assert result.status_code == status.HTTP_401_UNAUTHORIZED, result.content.decode()
        assert self.post.comments.count() == 0

    def test_can_reply_to_comment(self, authed_api_client):
        root_comment = CommentFactory(post=self.post)
        result = self._post_comment(
            authed_api_client(self.user),
            self.post,
            data={
                "content": self.content,
                "parent": root_comment.uuid,
            },
        )
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        root_comment.refresh_from_db()
        assert self.post.comments.count() == 2
        child_comment = first(root_comment.get_children())

        assert result.data["uuid"] == str(child_comment.uuid)

    def test_cant_reply_to_comment_and_set_other_post(self, authed_api_client):
        root_comment = CommentFactory()
        result = self._post_comment(
            authed_api_client(self.user),
            self.post,
            data={"content": self.content, "parent": root_comment.uuid},
        )

        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()

    def test_can_edit_comment(self, authed_api_client):
        comment = CommentFactory(post=self.post)
        result = self._edit_comment(
            authed_api_client(comment.user), comment, data={"content": self.content}
        )

        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        comment.refresh_from_db()
        assert result.data["content"] == self.content
        assert comment.content == self.content

    def test_cannot_edit_not_own_comment(self, authed_api_client):
        comment = CommentFactory(post=self.post)
        result = self._edit_comment(
            authed_api_client(self.user), comment, data={"content": self.content}
        )

        assert result.status_code == status.HTTP_403_FORBIDDEN, result.content.decode()

    def test_not_logged_in_cannot_edit_comment(self, anon_api_client):
        result = self._edit_comment(
            anon_api_client(), CommentFactory(), data={"content": self.content}
        )
        assert result.status_code == status.HTTP_401_UNAUTHORIZED, result.content.decode()

    def test_cannot_edit_comment_with_ratings(self, authed_api_client):
        comment = CommentFactory(post=self.post)
        CommentVoteFactory(comment=comment)
        result = self._edit_comment(
            authed_api_client(comment.user), comment, data={"content": self.content}
        )

        assert result.status_code in [
            status.HTTP_403_FORBIDDEN,
            status.HTTP_400_BAD_REQUEST,
        ], result.content.decode()

    def test_cannot_edit_comment_after_specific_time(self, authed_api_client, settings):
        minutes = 1
        settings.COMMENTS_EDITABLE_WINDOW_MINUTES = minutes
        comment = CommentFactory(post=self.post)
        now = timezone.now()
        with freeze_time(now + datetime.timedelta(minutes=minutes + 1)):
            result = self._edit_comment(
                authed_api_client(comment.user),
                comment,
                data={"content": self.content},
            )

        assert result.status_code == status.HTTP_403_FORBIDDEN, result.content.decode()

    def test_cannot_delete_comment(self, authed_api_client):
        result = self._delete_comment(
            authed_api_client(self.user),
            CommentFactory(user=self.user),
        )
        assert result.status_code == status.HTTP_403_FORBIDDEN, result.content.decode()

    @pytest.mark.parametrize("logged_in", (True, False))
    def test_cant_fetch_comments_without_post_uuid(
        self, logged_in, authed_api_client, anon_api_client
    ):
        client = authed_api_client(self.user) if logged_in else anon_api_client()
        CommentFactory(post=self.post)
        result = self._get_comments(client)
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert len(result.data) == 0

    @pytest.mark.parametrize("logged_in", (True, False))
    def test_can_fetch_children_comments_for_specific_comment(
        self, logged_in, authed_api_client, anon_api_client
    ):
        root_comment = CommentFactory(post=self.post)
        child_comment = CommentFactory(parent=root_comment, post=self.post)
        client = authed_api_client(self.user) if logged_in else anon_api_client()

        result = self._get_comments(
            client, post_slug=self.post.slug, parent_uuid=root_comment.uuid
        )

        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        data = result.data
        comment_from_api = first(data)

        assert comment_from_api["uuid"] == str(child_comment.uuid)

    @pytest.mark.parametrize("logged_in", (True, False))
    def test_no_comments_if_parent_comment_not_found(
        self, logged_in, authed_api_client, anon_api_client
    ):
        CommentFactory(parent=CommentFactory(post=self.post), post=self.post)
        client = authed_api_client(self.user) if logged_in else anon_api_client()
        result = self._get_comments(
            client, post_slug=self.post.slug, parent_uuid=uuid.uuid4()
        )
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert len(result.data) == 0

    @pytest.mark.parametrize("logged_in", (True, False))
    @pytest.mark.parametrize("expected_levels", (3, 5))
    def test_can_fetch_children_comments_up_to_level(
        self, expected_levels, logged_in, authed_api_client, anon_api_client
    ):
        comment = None
        for _ in range(expected_levels + 2):
            comment = CommentFactory(post=self.post, parent=comment, user=self.user)

        client = authed_api_client(self.user) if logged_in else anon_api_client()
        with patch(
            "comments.api.v1.views.get_user_default_comments_level",
            return_value=expected_levels,
        ):
            result = self._get_comments(client, post_slug=self.post.slug)

        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        children = first(result.data)
        levels = 0
        while children:
            children = first(children["children"])
            levels += 1
        assert levels - 1 == expected_levels

    def _get_comments(self, client, post_slug=None, parent_uuid=None):
        url = reverse("v1:comments:comments-list")
        params = {}
        if post_slug:
            params["post"] = post_slug
        if parent_uuid:
            params["parent"] = parent_uuid

        if params:
            url = f"{url}?{urlencode(params)}"

        return client.get(url)

    def _post_comment(self, client, post, data):
        data["post"] = post.slug
        return client.post(reverse("v1:comments:comments-list"), data=data)

    def _edit_comment(self, client, comment, data):
        return client.patch(
            reverse("v1:comments:comments-detail", kwargs={"uuid": comment.uuid}),
            data=data,
        )

    def _delete_comment(self, client, comment):
        return client.delete(
            reverse("v1:comments:comments-detail", kwargs={"uuid": comment.uuid}),
        )
