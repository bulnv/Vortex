from django_filters import rest_framework as filters

from tags.selectors import get_tags_for_autosuggestion


class TagsFilter(filters.FilterSet):
    s = filters.CharFilter(method="filter_by_name")  # noqa: VNE001

    def filter_by_name(self, queryset, name, value):
        return get_tags_for_autosuggestion(value)
