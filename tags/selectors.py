from django.conf import settings
from django.db.models import Q, QuerySet

from tags.models import Tag


def get_tags_for_autosuggestion(
    search: str, limit: int = settings.MAX_TAGS_TO_AUTOSUGGEST
) -> QuerySet[Tag]:
    return (
        Tag.objects.filter(name__icontains=search)
        .filter(Q(category__isnull=True) | Q(category__allowed_to_add_manually=True))
        .order_by("-usages")[:limit]
    )
