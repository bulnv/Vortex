from django.db import models
from django.utils.safestring import mark_safe


class Tag(models.Model):
    """Model to keep all created tags."""

    name = models.CharField(max_length=50, unique=True, db_index=True)
    category = models.ForeignKey(
        "tags.TagCategory",
        blank=True,
        null=True,
        related_name="tags",
        on_delete=models.SET_NULL,
    )
    usages = models.IntegerField(default=0)

    class Meta:
        verbose_name = "Тег"
        verbose_name_plural = "Теги"

    def __str__(self):
        return f"{self.name} - <{self.category}>"


class TagCategory(models.Model):
    """Model to categorize tags."""

    name = models.CharField("Имя", max_length=100, unique=True)
    # В случае если тег из этой категории использован, пост необходимо
    # отправить на модерацию
    send_post_for_moderation = models.BooleanField(
        "Отправить пост на модерацию",
        default=False,
        help_text=mark_safe(
            "В случае если пост содержит тег из этой категории, пост будет автоматически "
            "отправлен на модерацию"
        ),
    )
    # Теги из этой категории не могут быть назначены пользователем,
    # например [telegram]. Так же они не будут выводиться в autosuggest
    allowed_to_add_manually = models.BooleanField(
        "Может быть добавлен юзером",
        default=True,
        help_text=mark_safe(
            "Если чекбокс установлен, юзер сможет добавить тег при создании поста.<br/>"
            "Если чекбокс снят, теги из этой категории будут назначаться на основе "
            "бизнес-логики и юзер не сможет их добавить к любому посту.<br/>"
            "Например, при наличии ссылки на телеграм, длиннопост и т.д. <br/>"
            "Теги из такой категории не будет выводиться в подсказке к тегам при "
            "создании поста."
        ),
    )

    class Meta:
        verbose_name = "Категория тегов"
        verbose_name_plural = "Категории тегов"

    def __str__(self):
        return self.name
