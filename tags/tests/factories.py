import factory
from factory.django import DjangoModelFactory


class TagFactory(DjangoModelFactory):
    name = factory.Faker("word")

    class Meta:
        model = "tags.Tag"


class TagCategory(DjangoModelFactory):
    name = factory.Faker("word")

    class Meta:
        model = "tags.TagCategory"
