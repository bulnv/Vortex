import pytest
from rest_framework import status
from rest_framework.reverse import reverse

from posts.choices import PostStatus
from posts.models import PostView
from posts.tests.factories import PostFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestBulkPostViewViewset:
    def setup_method(self):
        self.posts = PostFactory.create_batch(
            size=2, status=PostStatus.PUBLISHED, views_count=0
        )
        self.viewer = UserPublicFactory()

    def test_cant_record_views_as_non_authed_user(self, anon_api_client):
        result = self._bulk_post_view(
            anon_api_client(), data={"views": [{"slug": self.posts[0].slug}]}
        )
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def test_can_record_views_as_authed_user(self, authed_api_client):
        data = {
            "views": [
                {
                    "slug": self.posts[0].slug,
                    "viewed_at": "2023-09-27 10:00:00",
                },
                {
                    "slug": self.posts[1].slug,
                },
            ]
        }
        # run it twice to make sure it is a) not failing, b) post view recorded once only
        for _ in range(2):
            result = self._bulk_post_view(authed_api_client(self.viewer), data=data)
            assert result.status_code == status.HTTP_201_CREATED
        for post in self.posts:
            post.refresh_from_db()
            assert post.views_count == 1
            assert PostView.objects.filter(post=post, user=self.viewer).exists()

    def _bulk_post_view(self, client, data):
        return client.post(reverse("v1:posts:post-bulk-views-list"), data=data)
