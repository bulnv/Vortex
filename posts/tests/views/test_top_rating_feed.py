import datetime

import pytest
from django.conf import settings
from django.utils import timezone
from rest_framework import status
from rest_framework.reverse import reverse

from posts.choices import PostFeedType, PostStatus, Vote
from posts.selectors import get_min_positive_votes_for_top_rating_feed
from posts.tests.factories import PostFactory, PostVoteFactory


@pytest.mark.django_db
class TestCommentedFeed:
    def setup_method(self):
        self.old_min_positive_votes = settings.POST_MIN_POSITIVE_VOTES_FOR_TOP_RATING_FEED
        self.old_min_percentage = (
            settings.POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TOP_RATING_FEED
        )
        settings.POST_MIN_POSITIVE_VOTES_FOR_TOP_RATING_FEED = 5
        settings.POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TOP_RATING_FEED = 80

        now = timezone.now()
        self.post_total_upvotes_100_percent = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=3)
        )
        self.post_total_upvotes_90_percent = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=2)
        )
        self.post_total_upvotes_50_percent = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=1)
        )

        min_positive_votes = get_min_positive_votes_for_top_rating_feed()

        PostVoteFactory.create_batch(
            size=min_positive_votes,
            value=Vote.UPVOTE,
            post=self.post_total_upvotes_100_percent,
        )

        PostVoteFactory.create_batch(
            size=min_positive_votes + 5,
            value=Vote.UPVOTE,
            post=self.post_total_upvotes_90_percent,
        )
        PostVoteFactory.create_batch(
            size=int((min_positive_votes + 5) * 0.1),
            value=Vote.DOWNVOTE,
            post=self.post_total_upvotes_90_percent,
        )

        PostVoteFactory.create_batch(
            size=min_positive_votes,
            value=Vote.UPVOTE,
            post=self.post_total_upvotes_50_percent,
        )
        PostVoteFactory.create_batch(
            size=min_positive_votes,
            value=Vote.DOWNVOTE,
            post=self.post_total_upvotes_50_percent,
        )

    def teardown_method(self):
        settings.POST_MIN_POSITIVE_VOTES_FOR_TOP_RATING_FEED = self.old_min_positive_votes
        settings.POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TOP_RATING_FEED = (
            self.old_min_percentage
        )

    def test_top_rating_feed(self, anon_api_client):
        result = self._get_posts(anon_api_client())
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        data = result.json()
        results = data["results"]
        ordering_from_api = [post["uuid"] for post in results]
        assert ordering_from_api == [
            str(self.post_total_upvotes_100_percent.uuid),
            str(self.post_total_upvotes_90_percent.uuid),
        ]

    def _get_posts(self, client):
        return client.get(
            reverse("v1:posts:posts-list") + f"?feed_type={PostFeedType.RATING}"
        )
