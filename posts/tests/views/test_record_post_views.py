import pytest
from rest_framework.reverse import reverse

from posts.choices import PostStatus
from posts.models import PostView
from posts.tests.factories import PostFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestRecordPostViews:
    def setup_method(self):
        self.post = PostFactory(status=PostStatus.PUBLISHED)
        self.viewer = UserPublicFactory()

    def test_post_views_counter_not_increased_for_non_logged_in(self, anon_api_client):
        self._get_post(anon_api_client(), self.post)
        self.post.refresh_from_db()
        assert self.post.views_count == 0

    def test_post_views_counter_increased_for_logged_in(self, authed_api_client):
        self._get_post(authed_api_client(self.viewer), self.post)
        self.post.refresh_from_db()
        assert self.post.views_count == 1
        assert PostView.objects.filter(post=self.post, user=self.viewer).count() == 1

        # Viewing again as a same user should not increase the count
        self._get_post(authed_api_client(self.viewer), self.post)
        self.post.refresh_from_db()
        assert self.post.views_count == 1
        assert PostView.objects.filter(post=self.post, user=self.viewer).count() == 1

        # Viewing as a different user should increase the count
        other_user = UserPublicFactory()
        self._get_post(authed_api_client(other_user), self.post)
        self.post.refresh_from_db()
        assert self.post.views_count == 2
        assert PostView.objects.filter(post=self.post, user=other_user).count() == 1

        assert PostView.objects.filter(post=self.post).count() == 2

    def _get_post(self, client, post):
        return client.get(reverse("v1:posts:posts-detail", kwargs={"slug": post.slug}))
