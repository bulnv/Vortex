import pytest
from funcy import first
from rest_framework import status
from rest_framework.reverse import reverse

from posts.choices import PostStatus
from posts.tests.factories import PostFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestFilterByUser:
    def setup_method(self):
        self.poster = UserPublicFactory()
        self.published_post = PostFactory(user=self.poster, status=PostStatus.PUBLISHED)
        self.draft_post = PostFactory(user=self.poster, status=PostStatus.DRAFT)
        self.other_user_post = PostFactory(status=PostStatus.PUBLISHED)

    def test_can_be_filtered_by_user(self, anon_api_client):
        result = self._get_posts(anon_api_client(), self.poster.username)
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]
        assert len(posts) == 1
        assert first(posts)["slug"] == self.published_post.slug

    @pytest.mark.parametrize("username,expected_count", (("not-found", 0), ("", 2)))
    def test_empty_result_when_user_not_found_or_empty(
        self, username, expected_count, anon_api_client
    ):
        result = self._get_posts(anon_api_client(), username)
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]
        assert len(posts) == expected_count

    def _get_posts(self, client, username):
        return client.get(reverse("v1:posts:posts-list") + f"?username={username}")
