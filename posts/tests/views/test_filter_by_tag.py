import pytest
from funcy import first
from rest_framework import status
from rest_framework.reverse import reverse

from posts.choices import PostStatus
from posts.tests.factories import PostFactory
from tags.tests.factories import TagFactory


@pytest.mark.django_db
class TestFilterByTag:
    def setup_method(self):
        self.tag_to_search = TagFactory(name="to-search")
        self.post = PostFactory(status=PostStatus.PUBLISHED, tags=[self.tag_to_search])
        PostFactory(status=PostStatus.PUBLISHED, tags=["not-to-search"])

    def test_can_be_filtered_by_tag(self, anon_api_client):
        result = self._get_posts(anon_api_client(), self.tag_to_search.name)
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]
        assert len(posts) == 1
        assert first(posts)["slug"] == self.post.slug

    @pytest.mark.parametrize("tag,expected_count", (("not-found", 0), ("", 2)))
    def test_result_when_tag_not_found_or_empty(
        self, tag, expected_count, anon_api_client
    ):
        result = self._get_posts(anon_api_client(), tag)
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]
        assert len(posts) == expected_count

    def _get_posts(self, client, tag):
        return client.get(reverse("v1:posts:posts-list") + f"?tag={tag}")
