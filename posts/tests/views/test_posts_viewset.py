import datetime

import pytest
from django.utils.timezone import make_aware
from rest_framework import status
from rest_framework.reverse import reverse

from common.api.pagination import KapibaraCursorPagination
from posts.choices import PostStatus
from posts.tests.factories import PostFactory


@pytest.mark.django_db
class TestPostsViewSet:
    def setup_method(self):
        self.old_page_size = KapibaraCursorPagination.page_size
        KapibaraCursorPagination.page_size = 1

    def teardown_method(self):
        KapibaraCursorPagination.page_size = self.old_page_size

    def test_cursor_pagination(self, anon_api_client):
        post1 = PostFactory(
            status=PostStatus.PUBLISHED,
            published_at=make_aware(datetime.datetime(2021, 1, 1)),
        )
        post2 = PostFactory(
            status=PostStatus.PUBLISHED,
            published_at=make_aware(datetime.datetime(2022, 1, 1)),
        )
        post3 = PostFactory(
            status=PostStatus.PUBLISHED,
            published_at=make_aware(datetime.datetime(2023, 1, 1)),
        )

        client = anon_api_client()

        result = self._get_posts(client)

        prev_url, next_url = self._assert_paginaged_result_and_return_nav(post3, result)
        assert prev_url is None

        result = self._get_posts(client, next_url)
        prev_url, next_url = self._assert_paginaged_result_and_return_nav(post2, result)

        result = self._get_posts(client, next_url)
        prev_url, next_url = self._assert_paginaged_result_and_return_nav(post1, result)
        assert next_url is None

    def _assert_paginaged_result_and_return_nav(self, post, result):
        assert result.status_code == status.HTTP_200_OK
        data = result.json()
        assert len(data["results"]) == 1
        assert data["results"][0]["uuid"] == str(post.uuid)
        return data["previous"], data["next"]

    def _get_posts(self, client, cursor=""):
        return client.get(reverse("v1:posts:posts-list") + f"?cursor={cursor}")
