import datetime

import pytest
from django.utils import timezone
from freezegun import freeze_time
from rest_framework import status
from rest_framework.reverse import reverse

from posts.choices import PostStatus
from posts.selectors import get_tag_for_telegram
from posts.tests.factories import PostFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestMyPosts:
    def setup(self):
        self.title = "Заголовок"
        self.expected_slug = "zagolovok"
        self.tags = ["тест", "проверка", "проверка", None]
        self.content = [{"type": "html", "value": "Тест создания поста"}]
        self.data = {
            "title": self.title,
            "content": self.content,
            "tags": self.tags,
        }
        self.user = UserPublicFactory()

    def test_active_user_can_create_new_post(self, authed_api_client):
        result = self._create_post(authed_api_client(self.user), data=self.data)

        assert result.status_code == status.HTTP_201_CREATED
        data = result.data
        assert data["title"] == self.title
        assert data["content"] == self.content
        assert data["slug"] == self.expected_slug
        assert sorted(data["tags"]) == sorted(list(filter(None, set(self.tags))))

    def test_malicious_html_tags_in_post_content_gets_cleaned(self, authed_api_client):
        content_with_tags = (
            "Test <b>html</b>"
            "<script>alert(1);</script>"
            "<br/>"
            "<h1>h1 tag</h1>"
            "<h2>h2 tag</h2>"
            "<h3>h3 tag</h3>"
            "<h4>h4 tag</h4>"
            "<h5>h5 tag</h5>"
            "<h6>h6 tag</h6>"
            "<a href='https://kapi.bar'>link</a>"
            "<a href='ftp://kapi.bar'>link</a>"
        )
        expected_sanitized_content = (
            "Test <b>html</b>"
            "<br>"
            "h1 tag"
            "h2 tag"
            "h3 tag"
            "h4 tag"
            "h5 tag"
            "h6 tag"
            '<a href="https://kapi.bar" rel="noopener noreferrer nofollow">link</a>'
            '<a rel="noopener noreferrer nofollow">link</a>'
        )
        result = self._create_post(
            authed_api_client(self.user),
            {"title": "html", "content": [{"type": "html", "value": content_with_tags}]},
        )
        data = result.data
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        assert data["content"][0]["value"] == expected_sanitized_content

    def test_not_active_user_cant_create_new_post(self, authed_api_client):
        result = self._create_post(
            authed_api_client(UserPublicFactory(is_active=False)), data=self.data
        )

        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def test_not_logged_in_user_cant_create_new_post(self, anon_api_client):
        result = self._create_post(anon_api_client(), data=self.data)

        assert result.status_code == status.HTTP_401_UNAUTHORIZED, result.content.decode()

    def test_not_logged_in_user_cant_fetch_my_draft_or_deleted_posts(
        self, anon_api_client
    ):
        author = UserPublicFactory()
        PostFactory(status=PostStatus.DRAFT, user=author)
        PostFactory(status=PostStatus.DELETED, user=author)
        result = self._fetch_my_posts(anon_api_client(), author.username)
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert len(result.data["results"]) == 0

    @pytest.mark.parametrize("empty_field", ("title", "content"))
    def test_need_required_data_to_create_post(self, authed_api_client, empty_field):
        data = self.data.copy()
        data[empty_field] = ""
        result = self._create_post(authed_api_client(self.user), data=data)

        assert result.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.parametrize(
        "content", ("", [], "test", {"type": "html", "value": "comment"})
    )
    def test_cant_post_with_malformed_content(self, content, authed_api_client):
        data = self.data.copy()
        data["content"] = content
        result = self._create_post(authed_api_client(self.user), data=data)

        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()

    def test_can_edit_own_post(self, authed_api_client):
        post = PostFactory(user=self.user)
        title = "edited post"
        tags = ["new tag", "one more"]
        content = [{"type": "html", "value": "edited post"}]
        result = self._edit_post(
            authed_api_client(self.user),
            post,
            {"title": title, "tags": tags, "content": content},
        )

        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        data = result.data
        assert data["title"] == title
        assert sorted(data["tags"]) == sorted(tags)
        assert data["content"] == content

    def test_cannot_edit_not_own_post(self, authed_api_client):
        post = PostFactory()
        result = self._edit_post(authed_api_client(self.user), post, {"title": "test"})
        assert result.status_code == status.HTTP_403_FORBIDDEN

    def test_cannot_edit_as_not_logged_in_user(self, anon_api_client):
        post = PostFactory()
        result = self._edit_post(anon_api_client(), post, {"title": "test"})
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def test_cannot_edit_as_not_active_user(self, authed_api_client):
        post = PostFactory(user=UserPublicFactory(is_active=False))
        result = self._edit_post(authed_api_client(post.user), post, {"title": "test"})
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def test_can_list_own_posts(self, authed_api_client):
        author = UserPublicFactory()
        my_posts = [
            PostFactory(status=PostStatus.PUBLISHED, user=author),
            PostFactory(status=PostStatus.DRAFT, user=author),
        ]
        PostFactory(status=PostStatus.PUBLISHED)

        result = self._fetch_my_posts(
            client=authed_api_client(author), username=author.username
        )

        assert result.status_code == status.HTTP_200_OK, result.content.decode()

        posts_from_api = result.data["results"]
        assert len(posts_from_api) == 2
        api_post_uuids = {post["uuid"] for post in posts_from_api}
        post_uuids = {str(post.uuid) for post in my_posts}
        assert api_post_uuids == post_uuids

    def test_cant_publish_not_own_post(self, authed_api_client):
        post = PostFactory(status=PostStatus.DRAFT)
        user = UserPublicFactory()

        client = authed_api_client(user)

        result = self._publish_post(client, post)

        assert result.status_code == status.HTTP_403_FORBIDDEN, result.content.decode()

    @pytest.mark.parametrize(
        "original_status,expected_status_code",
        (
            (PostStatus.DRAFT, status.HTTP_200_OK),
            (PostStatus.PUBLISHED, status.HTTP_200_OK),
            (PostStatus.DELETED, status.HTTP_400_BAD_REQUEST),
        ),
    )
    @freeze_time(datetime.datetime(2023, 8, 28, 10, 0, 0))
    def test_can_publish_certain_statuses(
        self, authed_api_client, original_status, expected_status_code, settings
    ):
        post = PostFactory(status=original_status)
        client = authed_api_client(post.user)
        result = self._publish_post(client, post)
        assert result.status_code == expected_status_code, result.content.decode()
        if expected_status_code != status.HTTP_400_BAD_REQUEST:
            post.refresh_from_db()
            now = timezone.now()
            assert result.data["status"] == PostStatus.PUBLISHED
            if original_status == PostStatus.DRAFT:
                assert result.data["published_at"] == now.strftime(
                    settings.REST_FRAMEWORK["DATETIME_FORMAT"]
                )

    @pytest.mark.parametrize(
        "post_status,expected_status_code",
        (
            (PostStatus.DRAFT, status.HTTP_200_OK),
            (PostStatus.PUBLISHED, status.HTTP_403_FORBIDDEN),
        ),
    )
    def test_edit_post_after_specific_time(
        self, post_status, expected_status_code, authed_api_client, settings
    ):
        minutes = 1
        settings.POSTS_EDITABLE_WINDOW_MINUTES = minutes
        post = PostFactory(status=post_status, published_at=timezone.now())
        now = timezone.now()
        with freeze_time(now + datetime.timedelta(minutes=minutes + 1)):
            result = self._edit_post(
                authed_api_client(post.user),
                post,
                data={"title": "new title"},
            )

        assert result.status_code == expected_status_code, result.content.decode()

    @pytest.mark.parametrize(
        "link,should_have_tag",
        (
            ("https://t.me/abc", True),
            ("https://abc.t.me/", True),
            ("https://abc.t.me", True),
            ("https://telegram.dog/abc", True),
            ("https://telegram.me/abc", True),
            ("https://cat.me/abc", False),
        ),
    )
    @pytest.mark.parametrize("has_telegram_tag_already", (False, True))
    def test_assign_telegram_tag_when_has_link_and_published(
        self, has_telegram_tag_already, link, should_have_tag, authed_api_client
    ):
        telegram_tag = get_tag_for_telegram()
        tags = [telegram_tag] if has_telegram_tag_already else []
        post = PostFactory(
            status=PostStatus.DRAFT,
            content=[
                {
                    "type": "text",
                    "value": f"some text with potential <a href='{link}'>link</a>",
                }
            ],
            tags=tags,
        )
        client = authed_api_client(post.user)
        result = self._publish_post(client, post)
        assert result.status_code == status.HTTP_200_OK

        tags = post.tags.values_list("name", flat=True)

        assert len(tags) == (1 if should_have_tag else 0)
        assert (telegram_tag.name in tags) is should_have_tag

    def _fetch_my_posts(self, client, username=None):
        return client.get(reverse("v1:posts:posts-list") + f"?username={username}")

    def _publish_post(self, client, post):
        return client.post(reverse("v1:posts:posts-publish", kwargs={"slug": post.slug}))

    def _create_post(self, client, data):
        return client.post(reverse("v1:posts:posts-list"), data=data)

    def _edit_post(self, client, post, data):
        return client.patch(
            reverse("v1:posts:posts-detail", kwargs={"slug": post.slug}), data=data
        )
