import datetime

import pytest
from django.utils import timezone
from rest_framework import status
from rest_framework.reverse import reverse

from comments.tests.factories import CommentFactory
from posts.choices import PostFeedType, PostStatus, TimePeriod
from posts.tests.factories import PostFactory


@pytest.mark.django_db
class TestCommentedFeed:
    def setup_method(self):
        now = timezone.now()
        self.post_0_comments = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=40)
        )
        self.post_2_comments = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=2)
        )
        self.post_1_comment = PostFactory(status=PostStatus.PUBLISHED, published_at=now)
        self.post_3_comments = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=8)
        )
        self.post_4_comments_same_date_as_3 = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=8)
        )
        CommentFactory(post=self.post_1_comment)
        CommentFactory.create_batch(size=2, post=self.post_2_comments)
        CommentFactory.create_batch(size=3, post=self.post_3_comments)
        CommentFactory.create_batch(size=4, post=self.post_4_comments_same_date_as_3)

    @pytest.mark.parametrize(
        "period,expected_ordering",
        (
            (
                "",
                [
                    "post_4_comments_same_date_as_3",
                    "post_3_comments",
                    "post_2_comments",
                    "post_1_comment",
                    "post_0_comments",
                ],
            ),
            (
                TimePeriod.ALL,
                [
                    "post_4_comments_same_date_as_3",
                    "post_3_comments",
                    "post_2_comments",
                    "post_1_comment",
                    "post_0_comments",
                ],
            ),
            (
                TimePeriod.MONTH,
                [
                    "post_4_comments_same_date_as_3",
                    "post_3_comments",
                    "post_2_comments",
                    "post_1_comment",
                ],
            ),
            (TimePeriod.WEEK, ["post_2_comments", "post_1_comment"]),
            (TimePeriod.DAY, ["post_1_comment"]),
        ),
    )
    def test_commented_feed(self, anon_api_client, period, expected_ordering):
        result = self._get_posts(anon_api_client(), period)
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        data = result.json()
        results = data["results"]
        expected_ordering = [str(getattr(self, post).uuid) for post in expected_ordering]
        ordering_from_api = [post["uuid"] for post in results]
        assert ordering_from_api == expected_ordering

    def _get_posts(self, client, period=""):
        if period:
            period = f"&period={period}"
        return client.get(
            reverse("v1:posts:posts-list")
            + f"?feed_type={PostFeedType.COMMENTED}{period}"
        )
