import factory
from django.utils import timezone
from factory.django import DjangoModelFactory

from posts.choices import PostStatus
from tags.models import Tag
from tags.tests.factories import TagFactory
from users.tests.factories import UserPublicFactory


class PostFactory(DjangoModelFactory):
    user = factory.SubFactory(UserPublicFactory)
    title = factory.Faker("text")
    content = factory.Faker("text")

    class Meta:
        model = "posts.Post"

    @factory.post_generation
    def populate_start_and_end_date(self, create, extracted, **kwargs):
        """Populate the start and end date if not populated."""
        if not create:
            return
        if self.status == PostStatus.PUBLISHED and not self.published_at:
            self.published_at = timezone.now()
            self.save(update_fields=["published_at"])

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted is None:
            return

        tags = []
        for tag in extracted:
            if isinstance(tag, str):
                tags.append(TagFactory(name=tag))
            elif isinstance(tag, Tag):
                tags.append(tag)
        self.tags.set(tags)


class PostVoteFactory(DjangoModelFactory):
    user = factory.SubFactory(UserPublicFactory)
    post = factory.SubFactory(PostFactory)

    class Meta:
        model = "posts.PostVote"


class PostViewFactory(DjangoModelFactory):
    user = factory.SubFactory(UserPublicFactory)
    post = factory.SubFactory(PostFactory)

    class Meta:
        model = "posts.PostView"
