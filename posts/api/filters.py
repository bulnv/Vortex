from django.utils import timezone
from django_filters import rest_framework as filters

from posts.choices import PostFeedType, PostStatus, TimePeriod
from posts.helpers import time_period_to_timedelta
from posts.selectors import (
    fetch_commented_posts,
    fetch_my_posts,
    fetch_rating_posts,
    fetch_user_posts,
)
from tags.models import Tag
from users.models import UserPublic


class PostFilter(filters.FilterSet):
    username = filters.CharFilter(method="filter_by_user")
    tag = filters.CharFilter(method="filter_by_tag")
    feed_type = filters.ChoiceFilter(
        choices=PostFeedType.choices, method="filter_by_feed_type"
    )
    period = filters.ChoiceFilter(choices=TimePeriod.choices, method="filter_by_period")
    status = filters.ChoiceFilter(choices=PostStatus.choices, method="filter_by_status")

    def filter_by_feed_type(self, queryset, name, value):
        user = self.request.user
        if value == PostFeedType.COMMENTED:
            return fetch_commented_posts(viewer=user, queryset=queryset)
        if value == PostFeedType.RATING:
            return fetch_rating_posts(viewer=user, queryset=queryset)
        return queryset

    def filter_by_period(self, queryset, name, value):
        if time_period := time_period_to_timedelta(value):
            from_date = timezone.now() - time_period
            return queryset.filter(published_at__gte=from_date)
        return queryset

    def filter_by_user(self, queryset, name, value):
        try:
            user = UserPublic.objects.get(username=value)
        except UserPublic.DoesNotExist:
            return queryset.none()

        if user == self.request.user:
            return fetch_my_posts(user)

        return fetch_user_posts(user=user, viewer=self.request.user, queryset=queryset)

    def filter_by_tag(self, queryset, name, value):
        try:
            tag = Tag.objects.get(name=value)
        except Tag.DoesNotExist:
            return queryset.none()
        return queryset.filter(tags=tag)

    def filter_by_status(self, queryset, name, value):
        return queryset.filter(status=value)


class PostFilterBackend(filters.DjangoFilterBackend):
    def get_ordering(self, request, queryset, view):
        feed_type = request.query_params.get("feed_type")
        if feed_type == PostFeedType.COMMENTED:
            return "-_comments_count", "-published_at"
        if feed_type == PostFeedType.RATING:
            return "-_positive_ratio", "-published_at"
        return "-published_at"
