from drf_spectacular.utils import extend_schema
from rest_access_policy import AccessViewSetMixin
from rest_framework import mixins, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from common.api.pagination import KapibaraCursorPagination
from posts.api.filters import PostFilter, PostFilterBackend
from posts.api.policies import PostAccessPolicy
from posts.api.serializers import (
    BulkPostViewSerializer,
    PostCreateSerializer,
    PostPublishSerializer,
    PostRatingOnlySerializer,
    PostSerializer,
    PostVoteCreateSerializer,
)
from posts.models import Post, PostView
from posts.selectors import fetch_posts
from posts.services import delete_post, publish_post, record_vote_for_post
from posts.tasks import record_bulk_views_task, record_post_view_task


class PostViewSet(AccessViewSetMixin, ModelViewSet):
    """API для управления постами."""

    serializer_class = PostSerializer
    lookup_field = "slug"
    access_policy = PostAccessPolicy
    pagination_class = KapibaraCursorPagination
    filter_backends = [PostFilterBackend]
    filterset_class = PostFilter

    def get_queryset(self):
        if self.action in ("publish", "update", "partial_update", "destroy"):
            return Post.objects.all()
        return fetch_posts(self.request.user)

    def get_serializer_class(self):
        """Return proper serializer based on action user performing."""
        if self.action in ("create", "update", "partial_update"):
            return PostCreateSerializer
        return self.serializer_class

    def retrieve(self, request, *args, **kwargs):
        post = self.get_object()
        serializer = self.get_serializer(post)
        user = request.user
        if user.is_authenticated:
            record_post_view_task.delay(user_id=user.pk, post_id=post.pk)
        return Response(serializer.data)

    @action(
        methods=["POST"],
        detail=True,
        queryset=Post.objects.all(),
        serializer_class=PostPublishSerializer,
    )
    def publish(self, request, *args, **kwargs):
        """Publish post and make it available on the website."""
        post = self.get_object()
        post = publish_post(post, request.user)
        return Response(self.get_serializer(post).data, status=status.HTTP_200_OK)

    def perform_destroy(self, instance: Post):
        """Delete user's own post."""
        delete_post(instance, self.request.user)

    @action(
        methods=["POST"],
        detail=True,
        serializer_class=PostVoteCreateSerializer,
    )
    def vote(self, request, *args, **kwargs):
        """Record vote for the post."""
        post = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        record_vote_for_post(post, request.user, serializer.data["value"])
        post.refresh_from_db()

        return Response(
            PostRatingOnlySerializer(post).data, status=status.HTTP_201_CREATED
        )


class BulkPostViewViewSet(GenericViewSet, mixins.CreateModelMixin):
    queryset = PostView.objects.all()
    http_method_names = ["post"]
    serializer_class = BulkPostViewSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        record_bulk_views_task.delay(
            bulk_views_data=serializer.data["views"], user_id=self.request.user.pk
        )

    @extend_schema(summary="Сохранение просмотра множества постов")
    def create(self, request, *args, **kwargs):
        super().create(request, *args, **kwargs)
        return Response(status=status.HTTP_201_CREATED)
