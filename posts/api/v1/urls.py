from rest_framework.routers import SimpleRouter

from posts.api.v1 import views

app_name = "posts"

router = SimpleRouter()
router.register("bulk-views", views.BulkPostViewViewSet, basename="post-bulk-views")
router.register("", views.PostViewSet, basename="posts")

urlpatterns = router.urls
