from rest_access_policy import AccessPolicy, Statement

from posts.selectors import can_create_post, can_edit_post, can_vote_for_post


class PostAccessPolicy(AccessPolicy):
    statements = [
        Statement(
            action=["list", "retrieve", "<safe_methods>"],
            principal="*",
            effect="allow",
        ),
        Statement(
            action=["create"],
            principal="authenticated",
            effect="allow",
            condition=["should_be_allowed_to_create_post"],
        ),
        Statement(
            action=["destroy", "publish"],
            principal="authenticated",
            effect="allow",
            condition=["user_must_be_author"],
        ),
        Statement(
            action=["update", "partial_update"],
            principal="authenticated",
            effect="allow",
            condition=[
                "user_must_be_author",
                "should_be_allowed_to_edit",
            ],
        ),
        Statement(
            action=["vote"],
            principal="authenticated",
            effect="allow",
            condition=["should_be_allowed_to_vote"],
        ),
    ]

    def user_must_be_author(self, request, view, action) -> bool:
        entity = view.get_object()
        return entity.user == request.user

    def should_be_allowed_to_edit(self, request, view, action) -> bool:
        return can_edit_post(request.user, view.get_object())

    def should_be_allowed_to_vote(self, request, view, action) -> bool:
        return can_vote_for_post(request.user, view.get_object())

    def should_be_allowed_to_create_post(self, request, view, action) -> bool:
        return can_create_post(request.user)
