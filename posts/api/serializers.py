import pydantic_core
from drf_spectacular.utils import OpenApiExample, extend_schema_serializer
from rest_framework import serializers

from common.api.fields import WritableSlugRelatedField
from common.schemas import Content
from posts.choices import Vote
from posts.models import Post, PostVote
from posts.selectors import can_edit_post
from tags.models import Tag
from users.api.serializers import UserPublicMinimalSerializer


class PostPublishSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = (
            "uuid",
            "slug",
            "status",
            "published_at",
        )
        read_only_fields = fields


class PostSerializer(serializers.ModelSerializer):
    """Serializer to represent Post instance."""

    author = UserPublicMinimalSerializer(source="user")
    tags = serializers.SlugRelatedField("name", many=True, read_only=True)
    comments_count = serializers.IntegerField(source="_comments_count")
    votes_up_count = serializers.IntegerField(source="_votes_up_count")
    votes_down_count = serializers.IntegerField(source="_votes_down_count")
    rating = serializers.FloatField(source="_rating")
    voted = serializers.ChoiceField(
        choices=Vote.choices, allow_null=True, source="_voted"
    )
    can_edit = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = (
            "uuid",
            "author",
            "title",
            "slug",
            "content",
            "post_group",
            "community",
            "tags",
            "views_count",
            "comments_count",
            "votes_up_count",
            "votes_down_count",
            "rating",
            "voted",
            "status",
            "published_at",
            "can_edit",
        )

    def get_can_edit(self, obj: Post):
        request = self.context.get("request")
        user = request and request.user
        return can_edit_post(user, obj)


class PostRatingOnlySerializer(serializers.ModelSerializer):
    """Serializer to return only rating related data."""

    class Meta:
        model = Post
        fields = (
            "uuid",
            "slug",
            "votes_up_count",
            "votes_down_count",
            "rating",
        )


@extend_schema_serializer(
    examples=[
        OpenApiExample(
            name="Создание поста",
            value={
                "title": "Мой супер-заголовок!",
                "content": [
                    {"type": "html", "value": "<p>А это сам пост</p>"},
                    {
                        "type": "img",
                        "src": "https://path.to/image.jpg",
                        "width": 300,
                        "height": 400,
                    },
                ],
                "tags": ["первый пост", "не судите строго", "капибара"],
            },
            request_only=True,
        ),
    ]
)
class PostCreateSerializer(serializers.ModelSerializer):
    """Serializer to accept and validate data to create Post."""

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    author = UserPublicMinimalSerializer(source="user", read_only=True)
    tags = WritableSlugRelatedField(
        slug_field="name",
        queryset=Tag.objects.all(),
        many=True,
        required=False,
        allow_null=True,
    )

    class Meta:
        model = Post
        fields = (
            "uuid",
            "user",
            "author",
            "title",
            "slug",
            "content",
            "tags",
            "status",
        )
        read_only_fields = (
            "uuid",
            "author",
            "slug",
            "status",
        )

    def validate_content(self, value):
        try:
            value = Content(content=value).model_dump(by_alias=True, exclude_none=True)
        except pydantic_core.ValidationError:
            raise serializers.ValidationError("Неверный формат данных")
        return value["content"]

    def validate_tags(self, value):
        return list(filter(None, value))


class PostVoteCreateSerializer(serializers.ModelSerializer):
    """Serializer validating data submitted to cast a vote on a post."""

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = PostVote
        fields = (
            "user",
            "value",
        )


class PostViewSerializer(serializers.Serializer):
    slug = serializers.SlugField()
    viewed_at = serializers.DateTimeField(required=False, allow_null=True)


class BulkPostViewSerializer(serializers.Serializer):
    views = PostViewSerializer(many=True)
