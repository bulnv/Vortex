from django.conf import settings
from django.db.models import F, FloatField, QuerySet
from django.db.models.functions import Cast
from django.utils import timezone

from posts.choices import PostStatus
from posts.models import Post, PostVote
from posts.querysets import PostQuerySet
from tags.models import Tag
from users.models import UserPublic


def fetch_posts(viewer: UserPublic) -> PostQuerySet:
    """Fetch pots which were added recently."""
    return (
        Post.published.order_by("-published_at")
        .with_ratings(viewer)
        .with_comments_count()
    )


def fetch_my_posts(user: UserPublic) -> QuerySet:
    """Fetch all user own posts, in all statuses."""
    return (
        user.posts.prefetch_related("tags")
        .select_related("user", "community")
        .with_ratings(user)
        .with_comments_count()
        .order_by(F("published_at").desc(nulls_last=True), "-created_at")
    )


def fetch_user_posts(
    user: UserPublic, viewer: UserPublic, queryset: PostQuerySet = None
) -> QuerySet:
    """Fetch all posts from the user."""
    queryset = fetch_posts(viewer) if queryset is None else queryset
    return queryset.filter(user=user)


def fetch_rating_posts(viewer: UserPublic, queryset: PostQuerySet = None) -> PostQuerySet:
    """Fetch posts with the highest rating."""
    queryset = fetch_posts(viewer) if queryset is None else queryset
    return (
        queryset.alias(
            _positive_ratio=Cast("_votes_up_count", FloatField())
            / (F("_votes_up_count") + F("_votes_down_count"))
        ).filter(
            _votes_up_count__gte=get_min_positive_votes_for_top_rating_feed(),
            _positive_ratio__gte=(
                get_min_positive_votes_percentage_for_top_rating_feed() / 100.0
            ),
        )
    ).order_by("-_positive_ratio")


def fetch_commented_posts(viewer: UserPublic, queryset: PostQuerySet = None) -> QuerySet:
    """Fetch posts with a lot of comments."""
    queryset = fetch_posts(viewer) if queryset is None else queryset
    return queryset.order_by("-_comments_count")


def get_post_editable_window_minutes() -> int:
    """Return for how many minutes since posting comment can be edited."""
    return settings.COMMENTS_EDITABLE_WINDOW_MINUTES


def get_post_vote_value_for_author(author: UserPublic, post_vote: PostVote) -> float:
    """Get value of how much rating post author should get on a single post vote."""
    return post_vote.value * settings.POST_RATING_MULTIPLIER


def can_edit_post(user: UserPublic, post: Post) -> bool:
    if user and user != post.user:
        return False
    if post.status != PostStatus.PUBLISHED or not post.published_at:
        return True
    now = timezone.now()
    return (
        now - post.published_at
    ).total_seconds() <= get_post_editable_window_minutes() * 60


def can_vote_for_post(user: UserPublic, post: Post) -> bool:
    # TODO: implement
    return True


def can_create_post(user: UserPublic) -> bool:
    # TODO: implement
    return True


def get_min_positive_votes_for_top_rating_feed() -> int:
    return settings.POST_MIN_POSITIVE_VOTES_FOR_TOP_RATING_FEED


def get_min_positive_votes_percentage_for_top_rating_feed() -> int:
    return settings.POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TOP_RATING_FEED


def get_tag_for_telegram() -> Tag:
    return Tag.objects.get(name="[телеграм]")
