import uuid

from django.db import models
from django.db.models import UniqueConstraint
from django_extensions.db.fields import AutoSlugField

from common.helpers import slugify_function
from common.models import Timestamped
from posts.choices import PostStatus, Vote
from posts.managers import PostsManager, PublishedPostsManager


class PostGroup(Timestamped):
    """Model to group posts into series."""

    name = models.CharField(max_length=100)
    slug = AutoSlugField(
        populate_from=["name"],
        editable=True,
        slugify_function=slugify_function,
        allow_duplicates=False,
        db_index=True,
    )
    user = models.ForeignKey(
        "users.UserPublic", on_delete=models.CASCADE, related_name="post_groups"
    )

    def __str__(self):
        return self.name


class Post(Timestamped):
    """Model represents Post entity."""

    uuid = models.UUIDField(
        default=uuid.uuid4, unique=True, db_index=True, editable=False
    )
    user = models.ForeignKey(
        "users.UserPublic", on_delete=models.CASCADE, related_name="posts"
    )
    post_group = models.ForeignKey(
        "posts.PostGroup",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="posts",
    )
    community = models.ForeignKey(
        "communities.Community",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="posts",
    )
    title = models.CharField(max_length=200)
    slug = AutoSlugField(
        populate_from=["title"],
        editable=True,
        slugify_function=slugify_function,
        allow_duplicates=False,
        db_index=True,
    )
    # need to add JSON schema validation, expected format is:
    # [
    #   {
    #       "type": "header",
    #       "value": "header content"
    #   },
    #   {
    #       "type": "paragraph",
    #       "value": "content with some <b>allowed</b> <em>tags</em>"
    #   }
    # ]
    content = models.JSONField()
    tags = models.ManyToManyField("tags.Tag", blank=True)
    views_count = models.IntegerField(default=0)
    comments_count = models.PositiveIntegerField(default=0)
    votes_up_count = models.PositiveIntegerField(default=0)
    votes_down_count = models.PositiveIntegerField(default=0)
    rating = models.IntegerField(default=0)
    status = models.CharField(
        max_length=9, choices=PostStatus.choices, default=PostStatus.DRAFT
    )
    published_at = models.DateTimeField(null=True, blank=True)

    objects = PostsManager()
    published = PublishedPostsManager()

    def __str__(self):
        return f"<{self.pk}: {self.title}>"


class PostVote(Timestamped):
    """Model to store post votes."""

    user = models.ForeignKey(
        "users.UserPublic", on_delete=models.CASCADE, related_name="post_votes"
    )
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="votes")
    value = models.SmallIntegerField(choices=Vote.choices)

    class Meta:
        constraints = [
            UniqueConstraint(fields=("user", "post"), name="user_post_vote"),
        ]


class PostView(models.Model):
    user = models.ForeignKey(
        "users.UserPublic", on_delete=models.CASCADE, related_name="post_views"
    )
    post = models.ForeignKey("posts.Post", on_delete=models.CASCADE, related_name="views")
    viewed_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        constraints = [
            UniqueConstraint(fields=("user", "post"), name="user_post_view"),
        ]
