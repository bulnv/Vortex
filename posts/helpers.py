import datetime

from posts.choices import TimePeriod


def time_period_to_timedelta(time_period: TimePeriod) -> datetime.timedelta | None:
    time_periods_map = {
        TimePeriod.DAY: datetime.timedelta(days=1),
        TimePeriod.WEEK: datetime.timedelta(days=7),
        TimePeriod.MONTH: datetime.timedelta(days=30),
        TimePeriod.ALL: None,
    }
    return time_periods_map.get(time_period)
