from django.db import models


class PostStatus(models.TextChoices):
    DRAFT = "draft", "Draft"
    PUBLISHED = "published", "Published"
    DELETED = "deleted", "Deleted"


class Vote(models.IntegerChoices):
    UPVOTE = 1
    DOWNVOTE = -1


class PostFeedType(models.TextChoices):
    COMMENTED = "commented"
    RATING = "rating"


class TimePeriod(models.TextChoices):
    DAY = "day"
    WEEK = "week"
    MONTH = "month"
    ALL = "all"
