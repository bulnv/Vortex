from django.conf import settings
from django.db.models import Case, Count, Exists, F, OuterRef, Q, QuerySet, Value, When

from posts.choices import Vote
from users.models import UserPublic


class PostQuerySet(QuerySet):
    def with_ratings(self, viewer: UserPublic):
        from posts.models import PostVote

        queryset = self.annotate(
            _votes_up_count=Count("votes", Q(votes__value=Vote.UPVOTE), distinct=True),
            _votes_down_count=Count(
                "votes", Q(votes__value=Vote.DOWNVOTE), distinct=True
            ),
            _rating=(F("_votes_up_count") - F("_votes_down_count"))
            * settings.POST_RATING_MULTIPLIER,
        )
        if viewer and viewer.is_authenticated:
            queryset = queryset.annotate(
                _voted=Case(
                    When(
                        Exists(
                            PostVote.objects.filter(
                                post=OuterRef("pk"), user=viewer, value=Vote.UPVOTE
                            )
                        ),
                        then=Value(Vote.UPVOTE),
                    ),
                    When(
                        Exists(
                            PostVote.objects.filter(
                                post=OuterRef("pk"), user=viewer, value=Vote.DOWNVOTE
                            )
                        ),
                        then=Value(Vote.DOWNVOTE),
                    ),
                )
            )
        return queryset

    def with_comments_count(self):
        return self.annotate(_comments_count=Count("comments", distinct=True))
