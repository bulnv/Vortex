import logging
import re

from posts.models import Post
from posts.selectors import get_tag_for_telegram
from tags.models import Tag

TELEGRAM_REGEX = re.compile(r"//([a-zA-Z0-9_-]+\.)?t(elegram)?.(me(/)?|dog/)")

logger = logging.getLogger(__name__)


class PostEvaluator:
    def __init__(self, post: Post):
        self.post = post

    def run(self):
        self.autoassign_tags()

    def autoassign_tags(self):
        self._process_telegram_tag()

    def _process_telegram_tag(self):
        try:
            telegram_tag = get_tag_for_telegram()
        except Tag.DoesNotExist:
            logger.error(
                "Cannot autotag post %s with telegram links, since tag for "
                "telegram doesn't exist",
                self.post,
            )
            return

        if self._has_telegram_links():
            self.post.tags.add(telegram_tag)
        else:
            self.post.tags.remove(telegram_tag)

    def _has_telegram_links(self) -> re.Match[str] | None:
        try:
            flatten_content = " ".join(
                [block.get("value", "") for block in self.post.content]
            )
            return TELEGRAM_REGEX.search(flatten_content)
        except AttributeError:
            pass
