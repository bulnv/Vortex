import contextlib

from celery import shared_task
from django.db import IntegrityError, transaction
from django.db.models import Count, F, OuterRef, Subquery

from posts.models import Post, PostView
from posts.services import record_bulk_views
from posts.types import BulkPostViewData
from users.models import UserPublic


@shared_task
def record_post_view_task(user_id: int, post_id: int):
    # TODO: кандидат на переработку. В идеале нужно не сразу записывать просмотры в базу,
    #  а хранить сначала в redis, по достижении определенного числа - сбрасывать в базу
    #  через PostView.bulk_create и затем обновлять количество просмотров самого поста
    with contextlib.suppress(IntegrityError), transaction.atomic():
        PostView.objects.create(user_id=user_id, post_id=post_id)
        Post.objects.filter(pk=post_id).update(views_count=F("views_count") + 1)


@shared_task
def record_bulk_views_task(bulk_views_data: BulkPostViewData, user_id: int):
    post_ids_to_sync = record_bulk_views(
        bulk_views_data, UserPublic.objects.get(pk=user_id)
    )
    sync_post_views_task(post_ids_to_sync)


@shared_task
def sync_post_views_task(post_ids: list[int] | None):
    posts_qs = (
        Post.published.alias(real_views_count=Count("views"))
        .exclude(views_count=F("real_views_count"))
        .filter(real_views_count__gt=0)
    )
    if post_ids is not None:
        posts_qs = posts_qs.filter(pk__in=post_ids)

    posts_qs.update(
        views_count=Subquery(
            PostView.objects.filter(post_id=OuterRef("pk"))
            .annotate(views_sum=Count("pk"))
            .values("views_sum")[:1]
        )
    )
